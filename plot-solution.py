# Copyright (C) 2016 Jack S. Hale, Paul Hauseux.
#
# This file is part of stochastic-burgers.
#
# stochastic-burgers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochastic-burgers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stochastic-burgers. If not, see <http://www.gnu.org/licenses/>.

import matplotlib as mpl
mpl.use("Agg")
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

sns.set_style("whitegrid")

x_ps = np.loadtxt('output/solution-points.txt')
mean = np.loadtxt('output/solution-mean.txt')
std_deviation = np.loadtxt('output/solution-std-deviation.txt')

interval = 1.959964
fig = plt.figure(figsize=((4.0, 3.2)))
ax = plt.subplot(111)
ax.fill_between(x_ps, mean + interval*std_deviation, mean - interval*std_deviation, interpolate=True, alpha=0.5)
ax.plot(x_ps, mean, label=r'$\bar{u}$')
ax.plot(x_ps, mean + interval*std_deviation, "--", label=r'$\bar{u} + 1.96\sigma$')
ax.plot(x_ps, mean - interval*std_deviation, "--", label=r'$\bar{u} - 1.96\sigma$')
ax.set_xlim([-0.5, 0.5])
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$\mathbb{E}[\psi(u)]$')
plt.legend(loc=2, frameon=True)

plt.tight_layout()
plt.savefig('output/solution.pdf')
print "Plot written to output/solution.pdf"
