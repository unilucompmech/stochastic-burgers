# Copyright (C) 2016 Jack S. Hale, Paul Hauseux.
#
# This file is part of stochastic-burgers.
#
# stochastic-burgers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochastic-burgers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stochastic-burgers. If not, see <http://www.gnu.org/licenses/>.
"""This file generates the root mean squared error test results.

Can take a long time to run. This checks that we get the correct O(1/sqrt(Z))
behaviour, and that the sensitivity derivative method does indeed produce a
lower error.
"""
import numpy as np
from progressbar import ProgressBar

from dolfin import set_log_level, WARNING
set_log_level(WARNING)

from estimate import estimate

# Number of realisations of the estimator
num_realisations = 100 
rmse_points = np.array([20, 50, 100, 500, 1000, 2000])
# NOTE: For testing script works.
#rmse_points = np.array([20, 50])
# Total number of forward model evaluations for progress bar
total_fm_evaluations = np.sum(num_realisations*rmse_points)

# 'Exact' answers.
exact = np.array([[0.64693769652], [0.418729810487], [0.64693769652], [0.418729810487]])

# The output of the estimator itself is a random variable.  Therefore we must
# estimate the error of the estimator using Monte Carlo. This involves taking
# multiple realisations of the same estimator. Namely, for each point on the
# RMSE graph we must generate num_realisations realisations of the estimator.
# Clearly, this involves a lot of model evaluations.
result = np.zeros([rmse_points.shape[0], 4])
with ProgressBar(max_value=total_fm_evaluations) as progress:
    for i, rmse_point in enumerate(rmse_points):
        # 4 estimates.
        output = np.zeros((num_realisations, 4))
        for j in range(0, num_realisations):
            output[j, :] = ((estimate(rmse_point) - exact)**2).reshape((-1))
            progress.update(progress.value + rmse_point)
        mc_estimate = np.mean(output, axis=0).reshape((-1,1))
        # A quick note about the array results. The columns of results
        # are the RMSE error of the estimator. The rows are the estimators
        # using increasing numbers of samples (rmse_points).
        result[i, :] = (np.sqrt(mc_estimate)/exact).reshape(-1)

# Save results in a file for plotting using plot-rmse-test.py
np.savetxt("output/rmse-points.txt", rmse_points)
np.savetxt("output/rmse-results.txt", result)
print "Saved results to files output/rmse-points.txt and output/rmse-results.txt."
