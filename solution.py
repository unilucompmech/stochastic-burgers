# Copyright (C) 2016 Jack S. Hale, Paul Hauseux.
#
# This file is part of stochastic-burgers.
#
# stochastic-burgers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochastic-burgers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stochastic-burgers. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from estimate import estimate

def main():
    """Generate mean and standard deviation for a set of domain points.
    
    After running you can plot with plot-solution.py
    """
    from dolfin import set_log_level, WARNING
    set_log_level(WARNING)
    
    samples = 2000
    x_ps = np.linspace(-0.5, 0.5, num=20)

    # Quantity of interest functionals as defined in paper
    # First moment
    psi_1 = lambda u: np.array([u(x_p) for x_p in x_ps])
    # Second moment
    # NOTE: Because of the chain rule trick used below you cannot just modify this
    # functional and expect it to work with the sensitivity derivative estimator.
    psi_2 = lambda u: np.array([(u(x_p))**2 for x_p in x_ps])

    result = estimate(samples, psi_1=psi_1, psi_2=psi_2)
    mean = result[2,:]
    second_moment = result[3,:]
    # NOTE: Please use a numerically stable algorithm for calculating the
    # standard deviation in production code!
    std_deviation = np.sqrt(second_moment - mean**2)

    np.savetxt('output/solution-points.txt', x_ps)
    np.savetxt('output/solution-mean.txt', mean)
    np.savetxt('output/solution-std-deviation.txt', std_deviation)


if __name__ == "__main__":
    main()
