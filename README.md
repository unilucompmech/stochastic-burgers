# stochastic-burgers 
## Solving the Stochastic Viscous Burgers Equation

### Description

This repository contains a code to solve the stochastic viscous Burgers equation using
a sensitivity derivative enhanced Monte Carlo method, shown in the paper:

Accelerating Monte Carlo estimation with derivatives of high-level finite element models, 
P. Hauseux, J.S. Hale, S.P.A. Bordas. 1 May 2017. 318, pp. 917-936. 10.1016/j.cma.2017.01.041.
Computer Methods in Applied Mechanics and Engineering.

http://orbilu.uni.lu/handle/10993/28618

http://dx.doi.org/10.1016/j.cma.2017.01.041

The code has a permanent DOI at https://dx.doi.org/10.6084/m9.figshare.3561306.v2

We kindly ask that you consider citing the paper and code if you find it useful for your work.

In this example we show improved convergence over the standard Monte Carlo method by over one order of
magnitude at the cost of just one extra linear solve *per estimation problem*.
The method could be combined with quasi-random Monte Carlo methods and other
variance reduction schemes for even better convergence.

![solution](https://bytebucket.org/unilucompmech/stochastic-burgers/raw/a8686fd118719dfa04e9a6fbb79a531337f76652/paper-output/solution.png?token=d6a05b92177c85c6f5e515803e56b06f17e7e070)

Above: Solution of stochastic Burgers equation.

### Key features 

* *Robust:* [FEniCS Project](https://fenicsproject.org) used to automatically derive
  and implement the finite element method solvers for the forward and tangent
  linear problems.

* *Fast:* We use a one-dimensional domain with 1024 cells so that each forward
  model realisation takes fractions of a second to run. This allows easy
  experimentation. But the code is perfectly able to handle larger more complex
  problems.
 
* *Concise:* Around 50 lines of Python to express and solve non-linear forward
  and tangent linear problems. Monte Carlo estimators around 80 lines of
  Python. Monte Carlo estimator code is independent of forward and tangent linear
  problem specification in FEniCS. 

* *Adaptable:* It should be possible to adapt this code to many different PDEs if you can
  express your problem in the Unified Form Language of the FEniCS Project. More
  advanced construction of tangent linear and adjoint models is available using
  [dolfin-adjoint](http://dolfin-adjoint.org).

* *Easy to run*: A Docker image is provided at `jhale/stochastic-burgers` to
  run the code in this repository. The result in the paper was produced using
  the image hash:

        sha256:2f8e65fcc27091fcbbda52b7b5d7e2f07d67be6bdccdc6551210111391c92d4c

### Note

Monte Carlo estimators are inherently random processes. You should not expect
*exactly* the same answer as the paper when you run this code. However the
overall trends (convergence rate, superior performance of the sensitivity
derivative estimator) should be evident. 

### References

See the paper at http://hdl.handle.net/10993/28618.

### Instructions

1. Install Docker for your platform following the instructions at
   [Docker.com](https://docker.com).

2. Clone this repository:

        git clone https://bitbucket.org/unilucompmech/stochastic-burgers

3. Enter the directory of the repository, and launch the Docker container using
   the command:

        cd stochastic-burgers
        ./launch-container.sh

4. You should be presented with a terminal inside the container. The main file
   that runs the Monte Carlo estimation procedure  is `estimate.py`. You can
   run it using:

        python estimate.py

5. By default, `estimate.py` generates 2000 samples. The results of the
   standard and sensitivity derivative Monte Carlo estimator are printed to the
   screen, along with a very accurate result we pre-generated using Chaospy.

6. For rigorous error convergence plots of the stochastic problem, run:

        python rmse-test.py

7. And then generate a plot of the results with:

        python plot-rmse-test.py

### Description of files

All `.py` files do sensible things when run with:

    python <file.py>

* `solution.py`: Calculate stochastic solution at a set of points in the
  domain. Outputs three files to `output/` for plotting with
  `plot-solution.py`.

* `estimate.py`: Implementation of the standard and sensitivity derivative
  Monte Carlo estimator. Calls forward and tangent linear models in
  `forward_tlm_models.py`.

* `forward_tlm_models.py` DOLFIN/UFL implementation of forward and tangent
  linear models of the viscous Burgers equation. Running the script with
  `python forward_tlm_models.py` will test the correct convergence of both
  models.

* `rmse-test.py`: This file calculates the root mean squared error of the two
  estimation approaches at a single domain point. Outputs two files to
  `output/` for plotting with `plot-rmse.py`.

* `plot-rmse.py`: This file plots the root mean squared error plot as shown 
  in the accompanying paper. Outputs PDF file to `output/`. 

* `plot-solution.py`: Plot the output of `solution.py` as shown in the 
  accompanying paper.

* `reference-solution.py`: This file uses Chaospy to generate an 'analytical'
  answer to the stochastic problem using the analytical solution to the Burgers
  equation. We have hard-coded our estimate of the 'analytical' solution into
  the other files, so it is not necessary to execute this file yourself.

* `launch-container.sh`: Launch the Docker container with pre-baked environment
  to execute code.

* `paper-output/*`: Raw results and generated plots from the paper.
    
### Issues and Support

Please use the [bugtracker](http://bitbucket.org/jhale/stochastic-burgers) to
report any issues.

For support or questions please email [jack.hale@uni.lu](mailto:jack.hale@uni.lu).

### Authors

Paul Hauseux, University of Luxembourg, Luxembourg.

Jack S. Hale, University of Luxembourg, Luxembourg.

### License 

stochastic-burgers is free software: you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with stochastic-burgers.  If not, see <http://www.gnu.org/licenses/>.