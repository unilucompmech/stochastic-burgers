# Copyright (C) 2016 Jack S. Hale, Paul Hauseux.
#
# This file is part of stochastic-burgers.
#
# stochastic-burgers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochastic-burgers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stochastic-burgers. If not, see <http://www.gnu.org/licenses/>.
import matplotlib as mpl
mpl.use("Agg")
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

sns.set_style("whitegrid")

rmse_points = np.loadtxt('output/rmse-points.txt')
results = np.loadtxt('output/rmse-results.txt')
rmse_u_mc = results[:, 0]
rmse_u_sd = results[:, 2]
rmse_u2_mc = results[:, 1]
rmse_u2_sd = results[:, 3]

# Line for comparison \mathcal{O}(1/\sqrt(Z))
tau_conv = np.array([1.0/np.sqrt(l) for l in rmse_points])

fig = plt.figure(figsize=(3, 2.2))
plt.grid(True)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('$Z$')
plt.ylabel('$\mathbb{E}^{\mathrm{RMSE}}$') 
plt.tick_params(axis='both', which='major')

colors = sns.color_palette("muted")

plt.plot(rmse_points, tau_conv , color=colors[4], label = "$\mathcal{O}(Z^{-1/2})$")
plt.plot(rmse_points, rmse_u_mc, marker='o', color=colors[0], label="$\mathbb{E}^{\mathrm{MC}}[\psi_1]}$")
plt.plot(rmse_points, rmse_u_sd, "--", marker='^', color =colors[0], label = "$\mathbb{E}^{\mathrm{SD-MC}}[\psi_1]}$")

plt.plot(rmse_points, rmse_u2_mc, marker='o', color = colors[1], label = "$\mathbb{E}^{\mathrm{MC}}[\psi_2]}$")
plt.plot(rmse_points, rmse_u2_sd, "--", marker='^', color = colors[1], label = "$\mathbb{E}^{\mathrm{SD-MC}}[\psi_2]}$") 
lgd = plt.legend(frameon=False, loc='upper center', bbox_to_anchor=(1.5,1.1), ncol=1, handlelength=4)
plt.tight_layout()
plt.savefig('output/rmse.pdf', bbox_extra_artists=(lgd,), bbox_inches='tight')
print "Plot written to output/rmse.pdf"
