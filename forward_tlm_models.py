# Copyright (C) 2016 Jack S. Hale, Paul Hauseux.
#
# This file is part of stochastic-burgers.
#
# stochastic-burgers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochastic-burgers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stochastic-burgers. If not, see <http://www.gnu.org/licenses/>.

"""Implementation of the forward and tangent linear models of the 1D
viscous Burgers equation in DOLFIN.

This file gives a template  with which you can express many different types of
steady-state PDEs using FEniCS and use in conjunction with the sensitivity
derivative Monte Carlo method.
"""

import numpy as np
from dolfin import *

# Number of divisions in mesh
n = 2**10

# Default global mesh for solving all realisations of forward and tangent
# linear model
mesh = UnitIntervalMesh(n)
mesh.coordinates()[:] -= 0.5
V = FunctionSpace(mesh, "CG", 1)

# Left and right domains for Dirichlet boundary conditions
left = CompiledSubDomain("near(x[0], side)", side=-0.5)
right = CompiledSubDomain("near(x[0], side)", side=0.5)

# Reference forward solution
u_e = Expression('0.5*(1.0 + tanh(x[0]/(4.0*nu)))', nu=0.0, degree=3)
# Reference tangent linear solution
dudnu_e = Expression('0.5*(x[0]/(4.0*nu*nu))*(pow(tanh(x[0]/(4.0*nu)), 2) - 1.0)', nu=0.0, degree=3)

def forward_full_model(nu, V=V):
    """Forward model of viscous Burgers equation.

    See also: forward(nu), a light wrapper around this function.

    Args:
        nu (float): Viscosity.
        V (dolfin.FunctionSpace): space on which to solve problem.
    
    Returns:
        The solution (dolfin.Function).
        Test function (dolfin.TestFunction).
        Trial function (dolfin.TrialFunction).
        Residual (ufl.Form).
        Parameter (ufl.variable)
    """
    u_ = Function(V)
    u = TrialFunction(V)
    u_t = TestFunction(V)

    u_e.nu = nu
    bc_left = DirichletBC(V, u_e(-0.5), left)
    bc_right = DirichletBC(V, u_e(0.5), right)
    bcs = [bc_left, bc_right]

    nu_var = variable(Constant(nu))

    F = nu_var*u_.dx(0)*u_t.dx(0)*dx + 0.5*u_.dx(0)*u_t*dx - 0.5*(u_**2).dx(0)*u_t*dx
    J = derivative(F, u_, u) 
    
    solve(F == 0, u_, bcs, J=J, 
          solver_parameters = {"newton_solver": {"relative_tolerance": 1e-14, "report": False}})
    
    return (u_, u, u_t, F, nu_var)


def forward_model(nu, **kwargs):
    """Returns just the solution from the function forward_full."""
    return forward_full_model(nu, **kwargs)[0]


def tangent_linear_model(nu, V=V):
    """Tangent linear model of viscous Burgers equation.

    Args:
        nu (float): Viscosity.
        V (dolfin.FunctionSpace): space on which to solve problem.

    Returns:
        The tangent linear solution (dolfin.Function).
    """
    # First we solve the forward model and get back all the necessary parts to
    # calculate the tangent linear model.
    u_, u, u_t, F, nu_var = forward_full_model(nu, V)

    # Now we construct the tangent linear model.
    dudnu_e.nu = nu
    bcl = DirichletBC(V, dudnu_e(-0.5), left)
    bcr = DirichletBC(V, dudnu_e(0.5), right)
    bcs = [bcl, bcr]
    
    du = TrialFunction(V) 
    # From definition of tangent linear model.
    a = derivative(F, u_, du)
    L = -diff(F, nu_var)
    
    dudnu = Function(V)
    A, b = assemble_system(a, L, bcs=bcs) 
   
    # Tangent linear model is always linear.
    solve(A, dudnu.vector(), b, "lu")
    
    return dudnu 


def main():
    """Runs a convergence test to check correctness of above models.
    
    This is a classical convergence test where we check that as we 
    refine the mesh the approximate solution converges to the exact
    one at the correct rate. We also check convergence across a range
    of viscosities.
    """
    # Range of mesh sizes
    ns = [2**j for j in range(2, 13)]
    nus = [0.25, 0.3, 0.1, 0.45, 0.2]
   
    set_log_level(WARNING)

    print "Running convergence test..."
    print "Cells: %s" % ns
    print "Viscosities: %s" % nus

    for nu in nus:
        errors_u = []
        errors_dudnu = []
        hs = []
        for n in ns:
            mesh = UnitIntervalMesh(n)
            mesh.coordinates()[:] -= 0.5
            
            hs.append(mesh.hmax())

            V = FunctionSpace(mesh, "CG", 1)
            V_e = FunctionSpace(mesh, "CG", 4)

            u = forward_model(nu, V=V)
            
            u_e_V_e = project(u_e, V_e)
            error_u = errornorm(u_e_V_e, u, norm_type="h1")/norm(u_e_V_e, norm_type="h1") 
            errors_u.append(error_u)

            # Clearly this is inefficient as we solve the forward problem
            # twice, but it works.
            dudm = tangent_linear_model(nu, V=V)
            dudm_e_V_e = project(dudnu_e, V_e)
            error_dudnu = errornorm(dudm_e_V_e, dudm, norm_type="h1")/norm(dudm_e_V_e, norm_type="h1")
            errors_dudnu.append(error_dudnu)
        
        # Using linear finite elements the rate of convergence should be O(h) for
        # both forward and tangent linear problems.
        z_forward = np.polyfit(np.log(hs), np.log(errors_u), 1)
        np.testing.assert_approx_equal(z_forward[0], 1.0, significant=2)

        z_tangent = np.polyfit(np.log(hs), np.log(errors_dudnu), 1)
        np.testing.assert_approx_equal(z_tangent[0], 1.0, significant=2)
    
    print "Acheived expected O(h) convergence in H^1-norm for forward and tangent linear models."
    print "Test passed."

if __name__ == "__main__":
    main()
