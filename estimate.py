# Copyright (C) 2016 Jack S. Hale, Paul Hauseux.
#
# This file is part of stochastic-burgers.
#
# stochastic-burgers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochastic-burgers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stochastic-burgers. If not, see <http://www.gnu.org/licenses/>.

"""Monte Carlo and Sensitivity Monte Carlo estimators implemented using
numpy.

The model in this case is the viscous Burgers equation, implemented
in forward_tlm_models.py.

Note that there is no FEniCS/DOLFIN code in this file. If your model
can output the required sensitivity derivative data, then you can work
with a modification of this code.

We also note that this code be extended to work with vector valued functionals.
This would allow full exploitation of the complete derivative calculated in the
tangent linear model of DOLFIN.
"""
import numpy as np
from forward_tlm_models import forward_model, tangent_linear_model

# Position of quantity of interest in domain
x_p = 0.3

# Quantity of interest functionals as defined in paper
# First moment
psi_1 = lambda u: np.array([u(x_p)])
# Second moment
# NOTE: Because of the chain rule trick used below you cannot just modify this
# functional and expect it to work with the sensitivity derivative estimator.
psi_2 = lambda u: np.array([(u(x_p))**2])

def estimate(num_samples, psi_1=psi_1, psi_2=psi_2):
    """Estimate using standard and sensivity derivative enhanced Monte Carlo.

    Args:
        num_samples (int): Number of samples.
    
    Returns:
        np.ndarray (float): Array of shape (4,) with 
    """
    # Draw samples from our known stochastic distribution
    # for the viscosity.
    omega_bar = 0.25
    omega_var = 0.025
    omegas = np.random.normal(omega_bar, omega_var, num_samples)
    
    # We store all of the functional samples. A better real-world implementation
    # for this would be to only store the current sample and the previous estimate
    # and do the Monte Carlo estimation as an update, e.g. an accumulator.
    
    # Dummy run, get shape of the output of the estimators.
    u = forward_model(omega_bar)
    psi_1_length = psi_1(u).shape[0]
    psi_2_length = psi_2(u).shape[0]
    
    # Space to store each sample.
    psi_1_zs = np.zeros([num_samples, psi_1_length])
    psi_2_zs = np.zeros([num_samples, psi_2_length])
    
    # Run forward mode, store output of quantity of interest.
    for i, omega in enumerate(omegas):
        u = forward_model(omega)
        # Rows are samples
        psi_1_zs[i, :] = psi_1(u)
        psi_2_zs[i, :] = psi_2(u)

    ## Standard Monte Carlo for comparison 
    def mc_estimator(psi_zs):
        E_psi = np.mean(psi_zs, axis=0)
        return E_psi

    # Two estimates from the standard Monte Carlo
    E_psi_1_mc = mc_estimator(psi_1_zs)
    E_psi_2_mc = mc_estimator(psi_2_zs)
   
    ## Sensitivity derivative Monte Carlo procedure.
    def sd_mc_estimator(psi_zs, dpsi_dm):
        correction = dpsi_dm*(omegas - omega_bar)[:, np.newaxis]
        E_psi = np.mean(psi_zs - correction, axis=0)
        return E_psi

    # Calculate derivative of the first moment with respect to parameter at the
    # mean value of the parameter. This would be more elegant with the adjoint
    # approach to automatic differentiation.
    dpsi_1_dm = psi_1(tangent_linear_model(omega_bar))
    # Sensitivity derivative estimate for the first derivative.
    E_psi_1_sd_mc = sd_mc_estimator(psi_1_zs, dpsi_1_dm)

    # Now calculate the derivative of the second moment with respect to the
    # parameter. We can do this without calculating another tangent linear
    # solution using the following chain rule 'trick'.
    dpsi_2_dm = 2.0*E_psi_1_sd_mc*dpsi_1_dm
    # Sensitivity derivative estimate for the second moment.
    E_psi_2_sd_mc = sd_mc_estimator(psi_2_zs, dpsi_2_dm)

    return np.array([E_psi_1_mc, E_psi_2_mc, E_psi_1_sd_mc, E_psi_2_sd_mc])

def main():
    """Quick run with 2000 samples, takes around 30 seconds."""
    # 'Exact' solutions computed using Chaospy
    E_psi_1_e = 0.646937696525
    E_psi_2_e = 0.418729810487

    from dolfin import set_log_level, WARNING
    set_log_level(WARNING)
    samples = 2000
    print "Estimating using %i samples..." % samples 
    result = estimate(2000)
    
    print result

if __name__ == "__main__":
    main()
